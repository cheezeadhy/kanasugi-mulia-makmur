require("./bootstrap");

import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbvue/lib/css/mdb.min.css";
import "nprogress/nprogress.css";
import router from "./routes";
import storeDefinition from "./store";
import App from "./app.vue";

window.Vue = require("vue").default;

Vue.config.productionTip = false;

//import VueTailwind from "vue-tailwind";
import VueRouter from "vue-router";
import VueAxios from "vue-axios";
import axios from "axios";
import NProgress from "nprogress";
import VueSweetalert2 from "vue-sweetalert2";
import Vuex from "vuex";

Vue.use(VueRouter);
Vue.use(VueSweetalert2);
Vue.use(VueAxios, axios);
Vue.use(NProgress);
Vue.use(Vuex);

//UNTUK MDBOOTSTRAP CAROUSEL (FUNGSI TOUCH PADA TOUCH SCREEN)
import Vue2TouchEvents from "vue2-touch-events";
Vue.use(Vue2TouchEvents);

//GLOBAL METHODS
Vue.mixin({
    methods: {
        thousandSeparator: function(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        },

        limitSentence: function(tString, tMaxLength) {
            let l = tString.length;
            let st = tString;
            if (l > tMaxLength) {
                st = tString.substring(0, tMaxLength) + "...";
            }
            return st;
        }
    }
});

router.beforeResolve((to, from, next) => {
    if (to.path) {
        NProgress.start();
    }
    next();
});

router.afterEach(() => {
    NProgress.done();
});

const store = new Vuex.Store(storeDefinition);

const app = new Vue({
    el: "#app",
    router,
    store,
    components: {
        index: App
    }
});
