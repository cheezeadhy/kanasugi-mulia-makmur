import VueRouter from "vue-router";

// PUBLIC PAGE
import home from "./components/pages/home.vue";
import jkregency from "./components/pages/jkregency.vue";
import tentangkami from "./components/pages/tentangkami.vue";
import testimony from "./components/pages/testimony.vue";
import faq from "./components/pages/faq.vue";
import hubungikami from "./components/pages/hubungikami.vue";

const routes = [
    {
        name: "home",
        path: "/",
        component: home,
    },
    {
        name: "jkregency",
        path: "/jkregency",
        component: jkregency,
    },
    {
        name: "tentangkami",
        path: "/tentangkami",
        component: tentangkami,
    },
    {
        name: "testimony",
        path: "/testimony",
        component: testimony,
    },
    {
        name: "faq",
        path: "/faq",
        component: faq,
    },
    {
        name: "hubungikami",
        path: "/hubungikami",
        component: hubungikami,
    },
];

const router = new VueRouter({
    mode: "history",
    routes: routes,
});

export default router;
