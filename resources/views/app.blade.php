<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Kanasugi Property adalah developer property yang berpengalaman dalam membangun berbagai jenis perumahan baik perumahan bersubsidi maupun komersial">
    <meta name="keywords" content="perumahan bersubsidi, rumah subsidi, perumahan bersubsidi lombok barat, rumah subsidi lombok barat, rumah subsidi lombok">
    <meta name="author" content="Bion Digital">
 
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- MDB icon -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Font Awesome -->
    {{-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css"> --}}
    <!-- Google Fonts Roboto -->
    {{-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"> --}}
    {{-- <link rel="stylesheet" href="{{ asset('css/style.css') }}"> --}}
    {{-- <link rel="stylesheet" href="{{ asset('css/app.css') }}"> --}}
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <title>Kanasugi Property | Perumahan Bersubsidi Lombok Barat</title>

</head>
<body>  
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<a href="https://api.whatsapp.com/send?phone=6287896468259&text=Hai%20tim%20Kanasugi%20Property%21%20Aku%20tertarik%20dan%20mau%20tau%20info%20lebih%20lanjut%20dong" class="float" target="_blank">
<i class="fa fa-whatsapp my-float"></i>
</a>

    <div id="app">
        <index></index>
    </div>
    
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>